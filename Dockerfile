FROM openjdk:11

RUN apt-get -y update
RUN apt-get -y install git
RUN apt -y install make
RUN apt -y install make-guile
RUN apt-get -y install gcc
RUN apt-get -y install g++
RUN apt-get -y install openssl
RUN apt-get -y install libssl-dev


WORKDIR /etc/demo

ADD build/libs/demo-0.0.1-SNAPSHOT.jar ./app.jar
ADD entrypoint.sh entrypoint.sh

RUN git clone https://github.com/unbound-tech/blockchain-crypto-mpc.git



CMD ["./entrypoint.sh"]