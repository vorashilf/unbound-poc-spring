## Unbounded MPC - Spring Boot wrapper

## How to run

Following command will build the Spring Boot application, compile C++ code and start 2 nodes
on port `8081` and `8082`

```
./build-run.sh build
```