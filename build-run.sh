#!/bin/bash

command=$1

if [ "$command" = "build" ]; then
  echo "Building the jar"
  ./gradlew clean build
fi

echo "Running the container"
docker compose down
docker compose up --build