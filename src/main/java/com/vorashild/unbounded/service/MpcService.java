package com.vorashild.unbounded.service;

import com.vorashild.unbounded.service.mpc.Context;
import com.vorashild.unbounded.service.mpc.MPCException;
import com.vorashild.unbounded.service.mpc.Native;
import com.vorashild.unbounded.service.mpc.Share;
import org.springframework.stereotype.Service;

@Service
public class MpcService {

    public String init() {

        try (Share node = new Share()) {
            return node.getInfo().toString();
        } catch (MPCException e) {
            throw new RuntimeException(e);
        }
    }
}
