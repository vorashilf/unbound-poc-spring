package com.vorashild.unbounded.controller;

import com.vorashild.unbounded.service.MpcService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class HelloWorld {

    private MpcService mpcService;

    @GetMapping("/hello")
    public String sayHello() {
        System.out.println(mpcService.init());
        return "Hello, World";
    }
}
