package com.vorashild.unbounded;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class UnboundPocSpringApplication {
	@Value("${spring.application.name}")
	private String appName;


	public static void main(String[] args) {
		SpringApplication.run(UnboundPocSpringApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		System.out.println("hello world, I am "+ appName);
	}

}
