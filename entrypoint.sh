#!/bin/bash

ls
cd blockchain-crypto-mpc || exit
make
export LD_LIBRARY_PATH=/etc/demo/blockchain-crypto-mpc
cd - || exit
java -jar app.jar